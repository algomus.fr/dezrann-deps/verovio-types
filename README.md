# Installation
> `npm install --save-dev  https://gitlab.com/algomus.fr/dezrann-deps/verovio-types.git

# Summary
This package contains custom type definitions for verovio (http://www.verovio.org) dev version.

# Details
Files were exported from https://gitlab.com/algomus.fr/dezrann-deps/verovio-types.git.

### Additional Details
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Benjamin Bloomfield](https://github.com/bbloomf).
